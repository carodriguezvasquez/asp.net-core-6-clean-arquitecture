﻿using MediatR;

namespace CleanArquitecture.Application.Behaviours
{
    public interface IValidationBehaviour<TRequest, TResponse>
    {
        Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next);
    }
}